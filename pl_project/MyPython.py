import sys
from typing import Dict

if len(sys.argv) <= 2:
    print("Please write your files name as argument in command line!\nTwo files as argument expected")
    sys.exit(0)

try:
    arg1 = sys.argv[1]
    arg2 = sys.argv[2]
except IndexError as error:
    print("There is something Wrong with your files!")
    sys.exit(0)

try:
    file1 = open(arg1, "r")
    file2 = open(arg2, "r")
except FileNotFoundError as error:
    print("File Not Found!!")
    sys.exit(0)
else:
    contents1 = file1.readlines()
    contents2 = file2.readlines()
    hash2df1: Dict[str, Dict[str, str]] = {}

    # load first file
    for phrase in contents1:
        phrase = phrase.replace("\n", "")
        word = phrase.split(" ")
        hash2df1[word[0]] = {word[1]: phrase}

    # reading second file
    for line in contents2:
        line = line.replace("\n", "")
        for key1 in hash2df1.keys():
            for key2 in hash2df1[key1].keys():
                phrase = hash2df1[key1][key2]
                if line.__contains__(phrase):
                    print("\"" + phrase + "\" exists in \"" + line + "\"")