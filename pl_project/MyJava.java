
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;


public class MyJava {
	public static void main(String[] args) {
		// print something if there is less than two files
		if (args.length <= 1) {
			System.out.println(
					"Please write your files name as argument in command line!\nTwo files as argument expected");
			System.exit(0);
		}

		try {
			// assign the name or the address of files in variables
			String arg0 = args[0];
			String arg1 = args[1];
			// call the second method
			main2(arg0, arg1);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("There is something Wrong with your files!");
			System.exit(0);
		}

	}

	// accept two string as arguments
	public static void main2(String arg0, String arg1) {
		File file1 = new File(arg0);
		File file2 = new File(arg1);
		// in order to have a higher speed of reeding we use Buffered reader
		
		try (BufferedReader br1 = new BufferedReader(new FileReader(file1));
				BufferedReader br2 = new BufferedReader(new FileReader(file2))) {
			// to store every single line per loop
			String line;
			// to store line after splitting
			String arr[];
			// load first file into two dimensional hash
			HashMap<String, HashMap<String, String>> hash2df1 = new HashMap<String, HashMap<String, String>>();
			while ((line = br1.readLine()) != null) {
				// split the line by space and assign it to an array
				arr = line.split(" ");
				// initialize the hash, name as a key and new hash as value
				hash2df1.put(arr[0], new HashMap<String, String>());
				// get or access the new hash and put second word as key
				hash2df1.get(arr[0]).put(arr[1], line);
			}
			

			//reading the second file and matching it with phrases.
			String firstkey = "";
			String secondkey = "";
			String phrase = "";
			while ((line = br2.readLine()) != null) {
				
				Iterator it = hash2df1.keySet().iterator();
				while(it.hasNext()) {
					firstkey  = (String) it.next();
					secondkey = hash2df1.get(firstkey).keySet().toString();
					secondkey = secondkey.substring(1, secondkey.length()-1);
					phrase = hash2df1.get(firstkey).get(secondkey);
					if(line.contains(phrase)){
						System.out.println("\"" + phrase + "\" exists in \"" + line + "\"");
					}
				}
			}

		} catch (FileNotFoundException e) {
			System.out.println("File Not Found!!");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
